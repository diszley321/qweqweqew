import React from 'react';
import styles from './header-styles.module.css';
import Indents from '../indents/indents';

export default function Header() {
    return (
        <div className={styles.header}>
            <Indents>
                Header
            </Indents>
        </div>
    );
}