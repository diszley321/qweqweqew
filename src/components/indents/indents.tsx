import React, {ReactNode} from 'react';
import styles from './indents-styles.module.css';

type IndentsPropsType = {
    children: ReactNode;
}

export default function Indents(props: IndentsPropsType) {
    return (
        <div className={styles.indents}>
            {props.children}
        </div>
    );
}