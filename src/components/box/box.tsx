import React, {ReactNode} from 'react';
import styles from './box-styles.module.css';

type IndentsPropsType = {
    children: ReactNode;
}

export default function Box(props: IndentsPropsType) {
    return (
        <div className={styles.box}>
            {props.children}
        </div>
    );
}