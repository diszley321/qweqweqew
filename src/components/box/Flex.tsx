import React, {ReactNode} from 'react';
import styles from './box-styles.module.css';

type IndentsPropsType = {
    children: ReactNode;
}

export default function Flex(props: IndentsPropsType) {
    return (
        <div className={styles.Flex}>
            {props.children}
        </div>
    );
}