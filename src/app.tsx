import React from 'react';
import Header from './components/header/header';
import Indents from './components/indents/indents';
import Box from './components/box/box';
import Flex from './components/box/Flex';

export default function App() {
    return (
        <>
            <Header/>
            <Indents>
            Hello
            </Indents>
            <Indents>
            <Flex>
            <Box>
                Box-1
            </Box>
            <Box>
                Box-2
            </Box>
            <Box>
                Box-3
            </Box>
                <Box>
                    Box-4
                </Box>
                <Box>
                    Box-5
                </Box>
                <Box>
                    Box-6
                </Box>
                <Box>
                    Box-7
                </Box>
                <Box>
                    Box-8
                </Box>
                <Box>
                    Box-9
                </Box>
                <Box>
                    Box-10
                </Box>
            </Flex>
            </Indents>

        </>
    );
}